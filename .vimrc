""git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
"""vundle.vimで管理しているpluginを読み込む"
source ~/dotfiles/.vimrc.bundle
"""基本設定"
source ~/dotfiles/.vimrc.basic
"""キーバインド"
source ~/dotfiles/.vimrc.binding
"""見た目の設定"
source ~/dotfiles/.vimrc.looking
"""テンプレート"
source ~/dotfiles/.vimrc.templates
